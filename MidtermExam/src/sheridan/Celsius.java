package sheridan;
/**
 * Jonathan Sriskandarajah 991531114
 */
import java.util.Scanner;

public class Celsius {

	public static int fromFahrenheit(int fahrenheit) {

		int celsius = ((fahrenheit - 32) * 5) / 9;
		
		return celsius;
	}
}
