package sheridan;
/**
 * Jonathan Sriskandarajah 991531114
 */
import static org.junit.Assert.*;

import org.junit.Test;
public class CelsiusTest {

	@Test
	void testFromFahrenheitRegular() {
		int result = Celsius.fromFahrenheit(50);
		assertTrue("Cannot convert Fahrenheit to Celsius", result == 10);
	}
	
	@Test(expected = NumberFormatException.class)
	void testFromFahrenheitException() {
		int result = Celsius.fromFahrenheit(0);
		fail("Cannot convert Fahrenheit to Celsius");
	}
	
	@Test(expected = NumberFormatException.class)
	void testFromFahrenheitBoundaryIn() {
		int result = Celsius.fromFahrenheit(00000);
		assertTrue("Cannot convert Fahrenheit to Celsius", result != 0);
	}
	
	
	@Test(expected = NumberFormatException.class)
	void testFromFahrenheitBoundaryOut() {
		int result = Celsius.fromFahrenheit(0-0);
		fail("Cannot convert Fahrenheit to Celsius");
	}

}
